var express = require('express')
var router = express.Router()
let controller = require('../controllers')

router.get('/', controller.getIndex)

module.exports = router
